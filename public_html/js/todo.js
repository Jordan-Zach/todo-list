$(function () {
    var APPLICATION_ID = "2291C062-74FE-D197-FF10-D601922CC100",
    SECRET_KEY = "5BBA8B56-CA95-862E-FF88-71044868DA00",
    VERSION = "v1";
    
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
  
    
    var user = new Backendless.User();
    user.email="jdzach2000@yahoo.com";
    user.password="password";
    Backendless.UserService.register(user); 
    
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
    console.log(postsCollection); 
    
    var wrapper = {
        posts: postsCollection.data
    };
    
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format("dddd, MMMM Do YYYY")
    });
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    
    $('.main-container').html(blogHTML);
    
    });
    
    function Posts(args) {
        args = args || {      };
        this.title = args.title || "";
        this.content = args.content || "";
        this.authorEmail = args.authorEmail || "";
    }